require 'coko_conversion/version'
require 'ink_step/conversion_step'

module InkStep::Coko
  class PdftkConversionStep < InkStep::ConversionStep

    def perform_step
      source_file_path = find_source_file(regex: /\.pdf$/)

      perform_pdftk_conversion(File.join(working_directory, source_file_path))
      success!
    end

    def version
      CokoConversion::VERSION
    end

    def self.description
      "PdfTK converter - template: pdftk <input file> <options>"
    end

    def self.human_readable_name
      "Generic PdfTK converter"
    end

    def required_parameters
      # e.g. [:foo, :bar]
      [:options]
    end

    def accepted_parameters
      # e.g. {foo: "For setting the grobblegronx measure", bar: "Can be X, Y or Z"}
      {
          options: "Any options for the conversion (see https://www.pdflabs.com/docs/pdftk-cli-examples/)"
      }
    end

    def default_parameter_values
      # e.g. {foo: 1, bar: nil}
      {options: ""}
    end

    private

    def perform_pdftk_conversion(source_file_path)
      # see readme for more info about usage
      # pdftk input_file <options>
      command = "pdftk #{Shellwords.escape(source_file_path)} #{parameter(:options)}"

      perform_command(command: command, error_prefix: "Error running pdftk")
    end
  end
end