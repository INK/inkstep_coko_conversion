require 'coko_conversion/version'
require 'ink_step/conversion_step'

module InkStep::Coko
  class CalibreConversionStep < InkStep::ConversionStep

    def perform_step
      raise ConversionError("Input file format must have a value") if parameter(:input_file_format).empty?
      raise ConversionError("Output file format must have a value") if parameter(:output_file_format).empty?
      source_file_path = find_source_file(regex: /\.#{parameter(:input_file_format)}$/)
      source_file_name = Pathname(source_file_path).sub_ext ''
      # provides the same file name with the new "output" extension
      # e.g. foo.html => foo.epub
      output_file_path = File.join(working_directory, "#{source_file_name}.#{parameter(:output_file_format)}")

      perform_calibre_conversion(File.join(working_directory, source_file_path), output_file_path)
      success!
    end

    def version
      CokoConversion::VERSION
    end

    def self.description
      "Generic Calibre converter - template: ebook-convert <input file> <output file> <options>"
    end

    def self.human_readable_name
      "Generic Calibre converter"
    end

    def required_parameters
      # e.g. [:foo, :bar]
      [:input_file_format, :output_file_format]
    end

    def accepted_parameters
      # e.g. {foo: "For setting the grobblegronx measure", bar: "Can be X, Y or Z"}
      {
          input_file_format: "Format of source file (guessing from the extension)",
          output_file_format: "Format of file to be written",
          options: "Any options for the conversion (see https://manual.calibre-ebook.com/generated/en/ebook-convert.html)"
      }
    end

    def default_parameter_values
      # e.g. {foo: 1, bar: nil}
      {options: ""}
    end

    private

    def perform_calibre_conversion(source_file_path, destination_file_path)
      # see readme for more info about usage
      # ebook-convert input_file output_file
      command = "ebook-convert #{Shellwords.escape(source_file_path)} #{Shellwords.escape(destination_file_path)} #{parameter(:options)}"

      perform_command(command: command, error_prefix: "Error running ebook-convert")
    end
  end
end