require 'coko_conversion/version'
require 'ink_step/conversion_step'

module InkStep::Coko
  class TidyConversionStep < InkStep::ConversionStep

    def perform_step
      source_file_path = find_source_file(regex: /\.html$/)

      perform_conversion(File.join(working_directory, source_file_path))
      success!
    end

    def version
      CokoConversion::VERSION
    end

    def self.description
      "Runs HTML Tidy terminal command against an HTML file. Template: tidy -m <input file> <options>"
    end

    def self.human_readable_name
      "Generic HTML Tidy converter"
    end

    def required_parameters
      # e.g. [:foo, :bar]
      [:options]
    end

    def accepted_parameters
      # e.g. {foo: "For setting the grobblegronx measure", bar: "Can be X, Y or Z"}
      {
          options: "Any options for the conversion (see http://www.html-tidy.org/documentation/)"
      }
    end

    def default_parameter_values
      # e.g. {foo: 1, bar: nil}
      {options: ""}
    end

    private

    def perform_conversion(source_file_path)
      # see readme for more info about usage
      # Modifies the supplied html file
      # tidy -m index.html
      command = "tidy -m #{Shellwords.escape(source_file_path)} #{parameter(:options)} "

      perform_command(command: command, error_prefix: "Error running tidy")
    end
  end
end