require 'coko_conversion/version'
require 'ink_step/conversion_step'
require 'coko_conversion/ink_step/coko/pandoc_conversion_step'

module InkStep::Coko
  class PandocDocxToHtmlStep < InkStep::Coko::PandocConversionStep

    def perform_step
      super
      success!
    end

    def version
      CokoConversion::VERSION
    end

    def self.human_readable_name
      "Pandoc Docx to HTML converter"
    end

    def self.description
      "Converts target .docx file to .html using pandoc"
    end

    def default_parameter_values
      # e.g. {foo: 1, bar: nil}
      {input_format: "docx", output_format: "html"}
    end
  end
end