require 'coko_conversion/version'
require 'ink_step/conversion_step'

module InkStep::Coko
  class CalibreHtmlToEpubStep < InkStep::ConversionStep

    def perform_step
      source_file_path = find_source_file(regex: parameter(:regex))
      source_file_name = Pathname(source_file_path).sub_ext ''
      output_file_path = File.join(working_directory, "#{source_file_name}.epub")

      perform_epub_conversion(File.join(working_directory, source_file_path), output_file_path)
      success!
    end

    def version
      CokoConversion::VERSION
    end

    def self.description
      "Converts target .html file to .epub"
    end

    def self.human_readable_name
      "Calibre HTML to EPUB converter"
    end

    def required_parameters
      # e.g. [:foo, :bar]
      []
    end

    def accepted_parameters
      # e.g. {foo: "For setting the grobblegronx measure", bar: "Can be X, Y or Z"}
      {regex: "Regular expression for input file"}
    end

    def default_parameter_values
      # e.g. {foo: 1, bar: nil}
      {regex: [/\.html$/, /\.htm$/]}
    end

    private

    def perform_epub_conversion(source_file_path, destination_file_path)
      # see readme for more info about usage
      # ebook-convert input_file output_file
      command = "ebook-convert #{Shellwords.escape(source_file_path)} #{Shellwords.escape(destination_file_path)}"

      perform_command(command: command, error_prefix: "Error running ebook-convert")
    end
  end
end