require 'coko_conversion/version'
require 'ink_step/conversion_step'
require 'coko_conversion/ink_step/coko/pandoc_conversion_step'

module InkStep::Coko
  class PandocEpubToHtmlStep < PandocConversionStep

    # One of many ways to make custom steps.
    # Subclassing allows access to parent class behaviours but can customise them more
    # than would be possible with just parameters.

    def perform_step
      super
      success!
    end

    def self.description
      "Converts target .epub file to .html via Pandoc"
    end

    def self.human_readable_name
      "Pandoc Epub to HTML converter"
    end

    def version
      CokoConversion::VERSION
    end

    def default_parameter_values
      # e.g. {foo: 1, bar: nil}
      {input_format: "epub", output_format: "html"}
    end
  end
end