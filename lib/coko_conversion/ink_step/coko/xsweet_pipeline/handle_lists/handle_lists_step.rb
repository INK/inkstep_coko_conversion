require 'coko_conversion/ink_step/coko/xsweet_pipeline/download_and_execute_xsl_via_saxon'

module InkStep::Coko
  module XsweetPipeline
    module HandleLists
      class HandleListsStep < XsweetPipeline::DownloadAndExecuteXslViaSaxon

        def perform_step
          source_file_relative_path = find_source_file(regex: parameter(:regex))
          source_file_path = File.join(working_directory, source_file_relative_path)


          log_as_step "marking lists from #{source_file_relative_path}"
          download_file(parameter(:mark_lists_xsl_uri))
          apply_xslt_transformation(input_file_path: source_file_path,
                                    output_file_path: lists_marked_path,
                                    xsl_file_path: xsl_file_path,
                                    provided_saxon_jar_path: nil)

          log_as_step "itemizing lists"
          download_file(parameter(:itemize_lists_xsl_uri))
          apply_xslt_transformation(input_file_path: lists_marked_path,
                                    output_file_path: source_file_path,
                                    xsl_file_path: xsl_file_path,
                                    provided_saxon_jar_path: nil)

          # cleanup
          delete_file(lists_marked_path)

          success!
        end

        def self.description
          "Handles lists"
        end

        def self.human_readable_name
          "List Handler"
        end

        def required_parameters
          [:itemize_lists_xsl_uri, :mark_lists_xsl_uri]
        end

        def accepted_parameters
          {
              itemize_lists_xsl_uri: "Location of raw XSL file to download - list itemiser",
              mark_lists_xsl_uri: "Location of raw XSL file to download - list marker",
              regex: "Regular expression for input file"
          }
        end

        def default_parameter_values
          {
              itemize_lists_xsl_uri: "https://gitlab.coko.foundation/XSweet/XSweet/raw/ink-api-publish/applications/list-promote/itemize-lists.xsl",
              mark_lists_xsl_uri: "https://gitlab.coko.foundation/XSweet/XSweet/raw/ink-api-publish/applications/list-promote/mark-lists.xsl",
              regex: [/\.html$/, /\.htm$/]
          }
        end

        def lists_marked_path
          File.join(working_directory, "lists_marked.html")
        end
      end
    end
  end
end
