require 'coko_conversion/ink_step/coko/xsweet_pipeline/download_and_execute_xsl_via_saxon'

module InkStep::Coko
  module XsweetPipeline
    module FinaliseTypescript
      class FinalRinseStep < XsweetPipeline::DownloadAndExecuteXslViaSaxon

        def perform_step
          super
          success!
        end

        def self.description
          "Tidies up the HTML by removing noise."
        end

        def self.human_readable_name
          "Xsweet Final Rinse"
        end

        def remote_xsl_location
          "https://gitlab.coko.foundation/XSweet/XSweet/raw/ink-api-publish/applications/html-polish/final-rinse.xsl"
        end
      end
    end
  end
end
