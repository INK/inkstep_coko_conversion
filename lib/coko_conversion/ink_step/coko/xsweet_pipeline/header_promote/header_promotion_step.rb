require 'coko_conversion/ink_step/coko/xsweet_pipeline/download_and_execute_xsl_via_saxon'

module InkStep::Coko
  module XsweetPipeline
    module HeaderPromote
      class HeaderPromotionStep < XsweetPipeline::DownloadAndExecuteXslViaSaxon

        def perform_step
          source_file_relative_path = find_source_file(regex: parameter(:regex))
          source_file_path = File.join(working_directory, source_file_relative_path)


          log_as_step "digesting paragraphs on #{source_file_relative_path} to make #{paragraphs_digested_path}..."
          download_file(parameter(:digest_paragraphs_xsl_uri))
          apply_xslt_transformation(input_file_path: source_file_path,
                                    output_file_path: paragraphs_digested_path,
                                    xsl_file_path: xsl_file_path,
                                    provided_saxon_jar_path: nil)

          log_as_step "creating header escalator xslt (#{header_escalator_xslt_path}..."
          download_file(parameter(:make_header_excalator_xslt_uri))
          apply_xslt_transformation(input_file_path: paragraphs_digested_path,
                                    output_file_path: header_escalator_xslt_path,
                                    xsl_file_path: xsl_file_path,
                                    provided_saxon_jar_path: nil)

          log_as_step "applying header escalator xslt on #{source_file_relative_path}..."
          apply_xslt_transformation(input_file_path: source_file_path,
                                    output_file_path: source_file_path,
                                    xsl_file_path: header_escalator_xslt_path,
                                    provided_saxon_jar_path: nil)

          # cleanup
          delete_file(paragraphs_digested_path)

          success!
        end

        def self.description
          "Detects headers by font size and promotes them to be headers (e.g. h2)"
        end

        def self.human_readable_name
          "Xsweet Header Promoter"
        end

        def required_parameters
          [:make_header_excalator_xslt_uri, :digest_paragraphs_xsl_uri]
        end

        def accepted_parameters
          {
              make_header_excalator_xslt_uri: "Location of raw XSL file to download - header escalator",
              digest_paragraphs_xsl_uri: "Location of raw XSL file to download - paragraph digester",
              regex: "Regular expression for input file"
          }
        end

        def default_parameter_values
          {
              make_header_excalator_xslt_uri: "https://gitlab.coko.foundation/XSweet/XSweet/raw/ink-api-publish/applications/header-promote/make-header-escalator-xslt.xsl",
              digest_paragraphs_xsl_uri: "https://gitlab.coko.foundation/XSweet/XSweet/raw/ink-api-publish/applications/header-promote/digest-paragraphs.xsl",
              regex: [/\.html$/, /\.htm$/]
          }
        end

        def header_escalator_xslt_path
          File.join(working_directory, "header_escalator.xslt")
        end

        def paragraphs_digested_path
          File.join(working_directory, "paragraphs_digested.html")
        end
      end
    end
  end
end
