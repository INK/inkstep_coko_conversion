require 'ink_step/conversion_step'
require 'coko_conversion/utilities/saxon_xsl_methods'
require 'ink_step/mixins/zip_methods'
require 'httparty'

module InkStep::Coko
  module XsweetPipeline
    class DownloadAndExecuteXslViaSaxonOnDocx < InkStep::ConversionStep
      include Utilities::SaxonXslMethods
      include InkStep::Mixins::ZipMethods

      attr_accessor :remote_xsl_uri

      def perform_step
        super
        @remote_xsl_uri = parameter(:remote_xsl_uri)
        source_file_path = find_source_file(regex: /\.docx$/)
        source_file_name = Pathname(source_file_path).sub_ext ''
        output_file_path = File.join(working_directory, "#{source_file_name}.html")
        log_as_step "converting #{source_file_path} to #{File.basename(output_file_path)}"

        unzip_archive(File.join(working_directory, source_file_path))
        download_file(remote_xsl_uri)

        apply_xslt_transformation(input_file_path: document_xml_path,
                                  xsl_file_path: xsl_file_path,
                                  output_file_path: output_file_path,
                                  provided_saxon_jar_path: nil)
      end

      def download_file(file_uri)
        @downloaded_file_name = filename_from_uri(file_uri)
        log_as_step "Downloading #{file_uri} to #{working_directory}..."
        downloaded_file = File.new(File.join(working_directory, @downloaded_file_name), "w")
        downloaded_file.write(HTTParty.get(file_uri).body)
        downloaded_file.close
      end

      def self.description
        "Downloads and applies the target XSL sheet URL against the target docx file"
      end

      def self.human_readable_name
        "XSL Executor via Saxon (docx only)"
      end

      def version
        CokoConversion::VERSION
      end

      def required_parameters
        # e.g. [:foo, :bar]
        [:remote_xsl_uri]
      end

      def accepted_parameters
        # e.g. {foo: "For setting the grobblegronx measure", bar: "Can be X, Y or Z"}
        {remote_xsl_uri: "The location of the raw XSL file to download"}
      end

      def default_parameter_values
        # e.g. {foo: 1, bar: nil}
        if self.respond_to?(:remote_xsl_location)
          {remote_xsl_uri: remote_xsl_location}
        else
          {}
        end
      end

      def xsl_file_path
        File.join(working_directory, @downloaded_file_name)
      end

      def filename_from_uri(uri)
        parsed_uri = URI.parse(uri)
        File.basename(parsed_uri.path)
      end
    end
  end
end