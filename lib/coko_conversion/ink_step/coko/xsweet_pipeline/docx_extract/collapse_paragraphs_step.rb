require 'coko_conversion/ink_step/coko/xsweet_pipeline/download_and_execute_xsl_via_saxon'

module InkStep::Coko
  module XsweetPipeline
    module DocxExtract
      class CollapseParagraphsStep < XsweetPipeline::DownloadAndExecuteXslViaSaxon

        def perform_step
          super
          success!
        end

        def remote_xsl_location
          "https://gitlab.coko.foundation/XSweet/XSweet/raw/ink-api-publish/applications/docx-extract/collapse-paragraphs.xsl"
        end

        def self.description
          "Identifies headers by font size"
        end

        def self.human_readable_name
          "Xsweet Paragraph Collapser"
        end
      end
    end
  end
end
