require 'coko_conversion/ink_step/coko/xsweet_pipeline/download_and_execute_xsl_via_saxon'

module InkStep::Coko
  module XsweetPipeline
    module DocxExtract
      class HandleNotesStep < XsweetPipeline::DownloadAndExecuteXslViaSaxon

        def perform_step
          super
          success!
        end

        def remote_xsl_location
          "https://gitlab.coko.foundation/XSweet/XSweet/raw/ink-api-publish/applications/docx-extract/handle-notes.xsl"
        end

        def self.description
          "Rearranges and cleans up footnotes. Lines up all the links and renumbers them in case they are out of order."
        end

        def self.human_readable_name
          "Xsweet Notes Handler"
        end
      end
    end
  end
end
