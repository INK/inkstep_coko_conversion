module CokoConversion
  if defined?(Rails)
    require 'coko_conversion/engine'
  else
    require 'coko_conversion/ink_step/vivliostyle_html_to_pdf_step'
    require 'coko_conversion/ink_step/wk_html_to_pdf_step'
    require 'coko_conversion/ink_step/calibre_html_to_epub_step'
    require 'coko_conversion/ink_step/pandoc_docx_to_html_step'
    require 'coko_conversion/ink_step/pandoc_conversion_step'
    require 'coko_conversion/ink_step/pandoc_epub_to_icml_step'

    require 'coko_conversion/ink_step/xsweet_pipeline/download_and_execute_xsl_via_saxon'
    require 'coko_conversion/ink_step/xsweet_pipeline/download_and_execute_xsl_via_saxon_on_docx'
    require 'coko_conversion/ink_step/xsweet_pipeline/docx_to_html_extract_step'
    require 'coko_conversion/ink_step/xsweet_pipeline/handle_notes_step'
    require 'coko_conversion/ink_step/xsweet_pipeline/join_elements_step'
    require 'coko_conversion/ink_step/xsweet_pipeline/scrub_step'
    require 'coko_conversion/ink_step/xsweet_pipeline/collapse_paragraphs'
  end
end