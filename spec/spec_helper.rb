require 'webmock/rspec'
require 'httparty'

RSpec.configure do |config|
  WebMock.disable_net_connect!(allow_localhost: true)
end

def temp_directory
  @temp_directory ||= "/tmp/test/#{Time.now.to_i}_#{SecureRandom.base64(6)}"
end

def create_directory_if_needed(path)
  FileUtils.mkdir_p(path) unless File.directory?(path)
end

def expect_successful_conversion(expected_output: nil, test_subject: subject)
  test_subject.input_file_manifest = test_subject.semantically_tagged_manifest

  test_subject.perform_step

  if expected_output
    expect(test_subject.semantically_tagged_manifest).to eq (expected_output)
  else
    expect(test_subject.semantically_tagged_manifest).to_not eq test_subject.input_file_manifest
  end
end