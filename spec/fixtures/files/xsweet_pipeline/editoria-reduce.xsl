<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns:xsw="http://coko.foundation/xsweet"
                xpath-default-namespace="http://www.w3.org/1999/xhtml"
                xmlns="http://www.w3.org/1999/xhtml"
                exclude-result-prefixes="#all">

  <!-- Note the default namespace for matching (given above) is
     "http://www.w3.org/1999/xhtml" -->

  <!-- The results will have XML syntax but no XML declaration or DOCTYPE declaration
       (as permitted by HTML5). -->

  <xsl:output method="xml" indent="no" omit-xml-declaration="yes"/>

  <!-- Just in case:
       <xsl:key name="elements-by-class" match="*[matches(@class,'\S')]"
       use="tokenize(@class,'\s+')"/>
  -->


  <!-- By default we *drop* elements.
       Better templates will copy the ones we want. -->

  <xsl:template match="*">
    <xsl:apply-templates/>
  </xsl:template>

  <!-- But we keep attributes -->
  <xsl:template match="@*">
    <xsl:copy-of select="."/>
  </xsl:template>

  <!-- Override for elements containing nothing but 'note' - remove them, keeping their contents. -->
  <xsl:template priority="5" match="body//*[empty((*|text()[normalize-space(.)]) except note)]">
    <xsl:apply-templates/>
  </xsl:template>

  <!-- Drop head/style -->
  <xsl:template priority="5" match="head/style"/>

  <xsl:template match="html | head | head//* | body">
    <xsl:apply-templates select="." mode="copy-after-all"/>
  </xsl:template>

  <xsl:template match="h1| h2 | h3 | h4 | h5 | h6">
    <xsl:apply-templates select="." mode="copy-after-all"/>
  </xsl:template>

  <xsl:template match="p | extract | blockquote | pre">
    <xsl:apply-templates select="." mode="copy-after-all"/>
  </xsl:template>

  <xsl:template match="b | i | sup | sub | a | code">
    <xsl:apply-templates select="." mode="copy-after-all"/>
  </xsl:template>

  <xsl:template match="note | note/@*">
    <xsl:apply-templates select="." mode="copy-after-all"/>
  </xsl:template>

  <xsl:template match="node() | @*" mode="copy-after-all">
    <xsl:copy>
      <!-- switching back out of mode -->
      <xsl:apply-templates select="node() | @*"/>
    </xsl:copy>
  </xsl:template>

  <!-- Bye-bye @class, bye-bye @style! -->
  <xsl:template match="@class | @style" priority="2"/>


</xsl:stylesheet>