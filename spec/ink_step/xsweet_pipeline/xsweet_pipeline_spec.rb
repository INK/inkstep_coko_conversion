require 'spec_helper'

require 'coko_conversion/ink_step/coko/xsweet_pipeline/download_and_execute_xsl_via_saxon'
require 'coko_conversion/ink_step/coko/xsweet_pipeline/docx_extract/docx_to_html_extract_step'
require 'coko_conversion/ink_step/coko/xsweet_pipeline/docx_extract/handle_notes_step'
require 'coko_conversion/ink_step/coko/xsweet_pipeline/docx_extract/scrub_step'
require 'coko_conversion/ink_step/coko/xsweet_pipeline/docx_extract/join_elements_step'
require 'coko_conversion/ink_step/coko/xsweet_pipeline/docx_extract/collapse_paragraphs_step'
require 'coko_conversion/ink_step/coko/xsweet_pipeline/handle_lists/handle_lists_step'
require 'coko_conversion/ink_step/coko/xsweet_pipeline/header_promote/header_promotion_step'
require 'coko_conversion/ink_step/coko/xsweet_pipeline/finalise_typescript/final_rinse_step'
require 'coko_conversion/ink_step/coko/xsweet_pipeline/prepare_for_editoria/editoria_prepare_step'

describe InkStep::Coko::XsweetPipeline::DownloadAndExecuteXslViaSaxon do
  
  def test_result(result_path: nil, expected_result_path:)
    subject.perform_step

    result_file = result_path || target_file_name
    result_path = File.join(subject.send(:working_directory), result_file)
    expect(File.read(result_path)).to eq File.read(expected_result_path)
  end

  let(:target_file)           { File.join(Dir.pwd, "spec", "fixtures", "files", "xsweet_pipeline", target_file_name) }
  let!(:input_directory)      { File.join(temp_directory, "input_files") }

  before do
    create_directory_if_needed(input_directory)
    FileUtils.cp(target_file, input_directory)
  end


  after :each do
    FileUtils.rm_rf(FileUtils.rm_rf(Dir.glob("#{temp_directory}/*")))
  end

  describe '#perform_step' do

    let(:remote_uri)           { subject.remote_xsl_location }

    describe 'single xsl steps' do
      before do
        [remote_uri].flatten.each do |uri|
          stub_request(:get, uri).
              with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>'Ruby'}).
              to_return(:status => 200, :body => xsl_file, :headers => {})
          FileUtils.cp(target_file, input_directory)
        end
      end

      describe 'Xsweet pipeline step 1' do
        let(:target_file_name)       { "basic_doc.docx" }
        let!(:xsl_file)              { File.read('spec/fixtures/files/xsweet_pipeline/docx-html-extract.xsl') }

        subject                      { InkStep::Coko::XsweetPipeline::DocxExtract::DocxToHtmlExtractStep.new(chain_file_location: temp_directory, position: 1) }

        let(:expected_manifest) {
          [
              {
              :path => "docx-html-extract.xsl",
              :size => "8.7 kB",
              :checksum => "97e34e755a802da78c206ff87eeaac1c",
              :tag => :new
          },
              {
              :path => "basic_doc.html",
              :size => "380 bytes",
              :checksum => "d8972a7e18099638c6ea037512717540",
              :tag => :new
          },
              {
              :path => "basic_doc/[Content_Types].xml",
              :size => "1.3 kB",
              :checksum => "ef7c7aab4ed1c89c4d286053052eac61",
              :tag => :new
          },
              {
              :path => "basic_doc/word/settings.xml",
              :size => "208 bytes",
              :checksum => "a4e1cd692809f2f552a8386c2f1d248f",
              :tag => :new
          },
              {
              :path => "basic_doc/word/styles.xml",
              :size => "2.3 kB",
              :checksum => "aee2c9c55cba7b33f39956161c387ddb",
              :tag => :new
          },
              {
              :path => "basic_doc/word/numbering.xml",
              :size => "4.8 kB",
              :checksum => "972926acd198e9688b89ed974ef9947a",
              :tag => :new
          },
              {
              :path => "basic_doc/word/document.xml",
              :size => "1.8 kB",
              :checksum => "a91ac4b6a40d38081f2a7d689c760166",
              :tag => :new
          },
              {
              :path => "basic_doc/word/fontTable.xml",
              :size => "853 bytes",
              :checksum => "8607336877646f51f438674727da21cb",
              :tag => :new
          },
              {
              :path => "basic_doc/word/_rels/document.xml.rels",
              :size => "664 bytes",
              :checksum => "5e0aa4a310402d3df359dc3db5b8eccc",
              :tag => :new
          },
              {
              :path => "basic_doc/docProps/app.xml",
              :size => "360 bytes",
              :checksum => "53781b1580dcf39cb563cbea6469b977",
              :tag => :new
          },
              {
              :path => "basic_doc/docProps/core.xml",
              :size => "505 bytes",
              :checksum => "e0636be1f07e192cde84087d93dd3f06",
              :tag => :new
          },
              {
              :path => "basic_doc.docx",
              :size => "4.8 kB",
              :checksum => "9e488bc7237c824490bdc707673971f1",
              :tag => :identical
          }
          ]
        }

        specify do
          subject.perform_step

          ap subject.send(:semantically_tagged_manifest)
          expect(subject.send(:semantically_tagged_manifest)).to match_array expected_manifest
        end

        describe '#version' do
          specify do
            expect{subject.version}.to_not raise_error
          end
        end

        describe '#description' do
          it 'has a description' do
            expect{subject.class.description}.to_not raise_error
          end
        end

        describe '#human_readable_name' do
          it 'has a human readable name' do
            expect{subject.class.human_readable_name}.to_not raise_error
          end
        end
      end


      describe 'Xsweet pipeline step 2' do
        let(:target_file_name)       { "xsweet_2_handle_notes_input.html" }

        let!(:html_file)             { 'spec/fixtures/files/xsweet_pipeline/xsweet_2_handle_notes_input.html' }
        let!(:xsl_file)              { File.read('spec/fixtures/files/xsweet_pipeline/handle-notes.xsl') }

        before do
          FileUtils.cp(html_file, input_directory)
        end

        subject {InkStep::Coko::XsweetPipeline::DocxExtract::HandleNotesStep.new(chain_file_location: temp_directory, position: 1)}

        let(:expected_manifest) {
          [
              {
              :path => "handle-notes.xsl",
              :size => "3.1 kB",
              :checksum => "5b325ef9e5386699bac2da37ea726f7d",
              :tag => :new
              },
              {
              :path => "xsweet_2_handle_notes_input.html",
              :size => "352 bytes",
              :checksum => "6950e09f5549bb3785bd0820679095ac",
              :tag => :modified
          }
          ]
        }

        specify do
          subject.perform_step

          expect(subject.send(:semantically_tagged_manifest)).to match_array expected_manifest
        end

        describe '#version' do
          specify do
            expect{subject.version}.to_not raise_error
          end
        end

        describe '#description' do
          it 'has a description' do
            expect{subject.class.description}.to_not raise_error
          end
        end

        describe '#human_readable_name' do
          it 'has a human readable name' do
            expect{subject.class.human_readable_name}.to_not raise_error
          end
        end
      end

      describe 'Xsweet pipeline step 3' do
        let(:target_file_name)       { "xsweet_3_scrub_input.html" }
        let!(:html_file)             { 'spec/fixtures/files/xsweet_pipeline/xsweet_3_scrub_input.html' }
        let(:xsl_file)               { File.read('spec/fixtures/files/xsweet_pipeline/scrub.xsl') }

        subject { InkStep::Coko::XsweetPipeline::DocxExtract::ScrubStep.new(chain_file_location: temp_directory, position: 1) }

        let(:expected_manifest) {
          [
              {
              :path => "scrub.xsl",
              :size => "2.1 kB",
              :checksum => "8b6f8e08b7f31f4353e8b1c67ad4bdc6",
              :tag => :new
              },
              {
              :path => "xsweet_3_scrub_input.html",
              :size => "314 bytes",
              :checksum => "cb27d0a582da95f78d1e0c8f5ffc5f34",
              :tag => :modified
            }
          ]
        }

        specify do
          subject.perform_step

          expect(subject.send(:semantically_tagged_manifest)).to match_array expected_manifest
        end

        describe '#version' do
          specify do
            expect{subject.version}.to_not raise_error
          end
        end

        describe '#description' do
          it 'has a description' do
            expect{subject.class.description}.to_not raise_error
          end
        end

        describe '#human_readable_name' do
          it 'has a human readable name' do
            expect{subject.class.human_readable_name}.to_not raise_error
          end
        end
      end

      describe 'Xsweet pipeline step 4' do
        let(:target_file_name)       { "xsweet_4_join_elements_input.html" }
        let!(:html_file)             { 'spec/fixtures/files/xsweet_pipeline/xsweet_4_join_elements_input.html' }
        let(:xsl_file)               { File.read('spec/fixtures/files/xsweet_pipeline/join-elements.xsl') }
        subject { InkStep::Coko::XsweetPipeline::DocxExtract::JoinElementsStep.new(chain_file_location: temp_directory, position: 1) }

        let(:expected_manifest) {
          [
            {
              :path => "xsweet_4_join_elements_input.html",
              :size => "327 bytes",
              :checksum => "91bbec4c2a9af04ffcb75608ddba1ce3",
              :tag => :modified
            },
            {
              :path => "join-elements.xsl",
              :size => "4.0 kB",
              :checksum => "53c5bdd99aa3217be0f86737b9441fac",
              :tag => :new
            }
          ]
        }

        specify do
          subject.perform_step

          expect(subject.send(:semantically_tagged_manifest)).to match_array expected_manifest
        end

        describe '#version' do
          specify do
            expect{subject.version}.to_not raise_error
          end
        end

        describe '#description' do
          it 'has a description' do
            expect{subject.class.description}.to_not raise_error
          end
        end

        describe '#human_readable_name' do
          it 'has a human readable name' do
            expect{subject.class.human_readable_name}.to_not raise_error
          end
        end
      end

      describe 'Xsweet pipeline step 5' do
        let(:target_file_name)       { "xsweet_5_collapse_paragraphs_input.html" }
        let(:html_file)              { 'spec/fixtures/files/xsweet_pipeline/xsweet_5_collapse_paragraphs_input.html' }
        let(:xsl_file)               { File.read('spec/fixtures/files/xsweet_pipeline/collapse-paragraphs.xsl') }

        subject { InkStep::Coko::XsweetPipeline::DocxExtract::CollapseParagraphsStep.new(chain_file_location: temp_directory, position: 1) }

        let(:expected_manifest) {
          [
              {
              :path => "collapse-paragraphs.xsl",
              :size => "1.4 kB",
              :checksum => "c5ccc795f71973e15719119d083ede7c",
              :tag => :new
          },
              {
              :path => "xsweet_5_collapse_paragraphs_input.html",
              :size => "332 bytes",
              :checksum => "6b776fed44e8355df79318f1ff3e1b70",
              :tag => :modified
          }
          ]
        }

        specify do
          subject.perform_step

          expect(subject.send(:semantically_tagged_manifest)).to match_array expected_manifest
        end

        describe '#version' do
          specify do
            expect{subject.version}.to_not raise_error
          end
        end

        describe '#description' do
          it 'has a description' do
            expect{subject.class.description}.to_not raise_error
          end
        end

        describe '#human_readable_name' do
          it 'has a human readable name' do
            expect{subject.class.human_readable_name}.to_not raise_error
          end
        end
      end

      # step 6 below

      describe 'Xsweet pipeline step 8' do
        let(:target_file_name)       { "xsweet_8_final_rinse_input.html" }
        let(:html_file)              { 'spec/fixtures/files/xsweet_pipeline/xsweet_8_final_rinse_input.html' }
        let(:xsl_file)               { File.read('spec/fixtures/files/xsweet_pipeline/final-rinse.xsl') }

        before do
          create_directory_if_needed(input_directory)
          FileUtils.cp(target_file, input_directory)
        end
        subject { InkStep::Coko::XsweetPipeline::FinaliseTypescript::FinalRinseStep.new(chain_file_location: temp_directory, position: 1) }

        let(:expected_manifest) {
          [
              {
              :path => "xsweet_8_final_rinse_input.html",
              :size => "5.4 kB",
              :checksum => "e29e54b9fb2ab295f46780c7137cc96f",
              :tag => :modified
          },
              {
              :path => "final-rinse.xsl",
              :size => "3.2 kB",
              :checksum => "72fa1fd861d7bcca34a4b0ae2c99c995",
              :tag => :new
          }
          ]
        }

        specify do
          subject.perform_step

          expect(subject.send(:semantically_tagged_manifest)).to match_array expected_manifest
        end

        describe '#version' do
          specify do
            expect{subject.version}.to_not raise_error
          end
        end

        describe '#description' do
          it 'has a description' do
            expect{subject.class.description}.to_not raise_error
          end
        end

        describe '#human_readable_name' do
          it 'has a human readable name' do
            expect{subject.class.human_readable_name}.to_not raise_error
          end
        end
      end

      # step 8 below
    end
  end

    # these ones are more complex with intermediary files

  describe 'more complex steps' do
    let(:uri_to_xsl_map) {
      {
          "https://gitlab.coko.foundation/XSweet/XSweet/raw/ink-api-publish/applications/header-promote/digest-paragraphs.xsl" => 'spec/fixtures/files/xsweet_pipeline/digest-paragraphs.xsl',
          "https://gitlab.coko.foundation/XSweet/XSweet/raw/ink-api-publish/applications/header-promote/make-header-escalator-xslt.xsl" => 'spec/fixtures/files/xsweet_pipeline/make-header-escalator-xslt.xsl',
          "https://gitlab.coko.foundation/XSweet/editoria_typescript/raw/ink-api-publish/editoria-notes.xsl" => 'spec/fixtures/files/xsweet_pipeline/editoria-notes.xsl',
          "https://gitlab.coko.foundation/XSweet/editoria_typescript/raw/ink-api-publish/editoria-basic.xsl" => 'spec/fixtures/files/xsweet_pipeline/editoria-basic.xsl',
          "https://gitlab.coko.foundation/XSweet/editoria_typescript/raw/ink-api-publish/editoria-reduce.xsl" => 'spec/fixtures/files/xsweet_pipeline/editoria-reduce.xsl',
          "https://gitlab.coko.foundation/XSweet/editoria_typescript/raw/ink-api-publish/p-split-around-br.xsl" => 'spec/fixtures/files/xsweet_pipeline/p-split-around-br.xsl',
          "https://gitlab.coko.foundation/XSweet/XSweet/raw/ink-api-publish/applications/list-promote/mark-lists.xsl" => 'spec/fixtures/files/xsweet_pipeline/mark-lists.xsl',
          "https://gitlab.coko.foundation/XSweet/XSweet/raw/ink-api-publish/applications/list-promote/itemize-lists.xsl" => 'spec/fixtures/files/xsweet_pipeline/itemize-lists.xsl'
      }
    }

    describe 'Xsweet pipeline step 6' do

      before do
        uris.each do |uri|
          stub_request(:get, uri).
              with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>'Ruby'}).
              to_return(:status => 200, :body => File.read(uri_to_xsl_map[uri]), :headers => {})
        end
        FileUtils.cp(target_file, input_directory)
      end

      let(:target_file_name)       { "xsweet_6_list_handle_input.html" }
      let(:uris)                   { subject.default_parameter_values.values.select{|v| v.is_a?(String) && v.match(/\.xsl$/)} }
      subject { InkStep::Coko::XsweetPipeline::HandleLists::HandleListsStep.new(chain_file_location: temp_directory, position: 1) }

      let(:expected_manifest) {
        [
            {
            :path => "itemize-lists.xsl",
            :size => "1.0 kB",
            :checksum => "d82f31bc35b86542f1b4ab4ce11e2664",
            :tag => :new
        },
            {
            :path => "mark-lists.xsl",
            :size => "3.7 kB",
            :checksum => "938114eabed7db3950412743061774fc",
            :tag => :new
        },
            {
            :path => "xsweet_6_list_handle_input.html",
            :size => "4.5 kB",
            :checksum => "cd614f86070aaac380ee00907f21c19e",
            :tag => :modified
        }
        ]
      }

      specify do
        subject.perform_step

        expect(subject.send(:semantically_tagged_manifest)).to match_array expected_manifest
      end

      describe '#version' do
        specify do
          expect{subject.version}.to_not raise_error
        end
      end

      describe '#description' do
        it 'has a description' do
          expect{subject.class.description}.to_not raise_error
        end
      end

      describe '#human_readable_name' do
        it 'has a human readable name' do
          expect{subject.class.human_readable_name}.to_not raise_error
        end
      end
    end

    describe 'Xsweet pipeline step 7' do

      before do
        uris.each do |uri|
          stub_request(:get, uri).
              with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>'Ruby'}).
              to_return(:status => 200, :body => File.read(uri_to_xsl_map[uri]), :headers => {})
        end
        FileUtils.cp(target_file, input_directory)
      end

      let(:target_file_name)       { "xsweet_7_header_promotion_input.html" }
      let(:uris)                   { subject.default_parameter_values.values.select{|v| v.is_a?(String) && v.match(/\.xsl$/)} }
      let(:html_file)              { 'spec/fixtures/files/xsweet_pipeline/xsweet_7_header_promotion_input.html' }
      let(:expected_result_path)   { 'spec/fixtures/files/xsweet_pipeline/xsweet_7_header_promotion_result.html' }
      let(:result_path)            { File.read(File.join(working_directory, target_file_name)) }
      subject { InkStep::Coko::XsweetPipeline::HeaderPromote::HeaderPromotionStep.new(chain_file_location: temp_directory, position: 1) }

      specify do
        expect(File.exists?(subject.paragraphs_digested_path)).to be_falsey
      end

      let(:expected_manifest) {
        [
            {
            :path => "header_escalator.xslt",
            :size => "2.2 kB",
            :checksum => "ee003294eb8723248d07102b1423c1cf",
            :tag => :new
        },
            {
            :path => "digest-paragraphs.xsl",
            :size => "10.1 kB",
            :checksum => "25f087399d44d0cab927e4198ab10fad",
            :tag => :new
        },
            {
            :path => "make-header-escalator-xslt.xsl",
            :size => "3.3 kB",
            :checksum => "706fbf492b174648059338b94f2e877b",
            :tag => :new
        },
            {
            :path => "xsweet_7_header_promotion_input.html",
            :size => "6.4 kB",
            :checksum => "e7f4739b7123fc0fed722f053464f3c1",
            :tag => :modified
        }
        ]
      }

      specify do
        subject.perform_step

        expect(subject.send(:semantically_tagged_manifest)).to match_array expected_manifest
      end

      describe '#version' do
        specify do
          expect{subject.version}.to_not raise_error
        end
      end

      describe '#description' do
        it 'has a description' do
          expect{subject.class.description}.to_not raise_error
        end
      end

      describe '#human_readable_name' do
        it 'has a human readable name' do
          expect{subject.class.human_readable_name}.to_not raise_error
        end
      end
    end

    describe 'Xsweet pipeline step 9' do
      before do
        uris.each do |uri|
          stub_request(:get, uri).
              with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>'Ruby'}).
              to_return(:status => 200, :body => File.read(uri_to_xsl_map[uri]), :headers => {})
        end
        FileUtils.cp(target_file, input_directory)
      end

      let(:target_file_name)       { "xsweet_9_prepare_editoria_input.html" }
      let(:uris)                   { subject.default_parameter_values.values.select{|v| v.is_a?(String) && v.match(/\.xsl$/)} }
      let(:html_file)              { 'spec/fixtures/files/xsweet_pipeline/xsweet_9_prepare_editoria_input.html' }
      let(:expected_result_path)   { 'spec/fixtures/files/xsweet_pipeline/xsweet_9_prepare_editoria_result.html' }
      let(:result_path)            { File.read(File.join(working_directory, target_file_name)) }
      subject { InkStep::Coko::XsweetPipeline::PrepareForEditoria::EditoriaPrepareStep.new(chain_file_location: temp_directory, position: 1)}

      let(:expected_manifest) {
        [
            {path: "p_split_on_br_done.html", size: "4.8 kB", checksum: "a84c3950fba645fed19b8e93787f9324", tag: :new},
            {path: "p-split-around-br.xsl", size: "3.1 kB",
             checksum: "cf9049e720b3c7ee596ff798d30fbe24",
             tag: :new},
            {path: "editoria-basic.xsl",
               size: "2.7 kB",
               checksum: "f000f64edc490c4d879409aa22ad2b2b",
               tag: :new},
            {path: "basic_done.html",
             size: "4.7 kB",
             checksum: "c4f27f21e141430a92494a7be9f6e190",
             tag: :new},
            {path: "editoria-notes.xsl",
               size: "2.3 kB",
               checksum: "5b4e8f6c4b1e235afc6e12c3658e5f19",
               tag: :new},
            {path: "editoria-reduce.xsl",
               size: "2.4 kB",
               checksum: "9f2887fcee329b7e4594a6117b3d7097",
               tag: :new},
            {path: "xsweet_9_prepare_editoria_input.html",
               size: "4.4 kB",
               checksum: "5b2e2a5fa7f1bb7c2150f832062cc141",
               tag: :modified},
            {path: "notes_done.html",
               size: "4.7 kB",
               checksum: "c4f27f21e141430a92494a7be9f6e190",
               tag: :new}
        ]
      }

      specify do
        subject.perform_step

        expect(subject.send(:semantically_tagged_manifest)).to match_array expected_manifest
      end

      describe '#version' do
        specify do
          expect{subject.version}.to_not raise_error
        end
      end

      describe '#description' do
        it 'has a description' do
          expect{subject.class.description}.to_not raise_error
        end
      end

      describe '#human_readable_name' do
        it 'has a human readable name' do
          expect{subject.class.human_readable_name}.to_not raise_error
        end
      end
    end
  end

  describe '#methods' do
    let(:target_file_name)       { "xsweet_9_prepare_editoria_input.html" }
    subject { InkStep::Coko::XsweetPipeline::DownloadAndExecuteXslViaSaxon.new(chain_file_location: temp_directory,position: 1) }

    describe '#version' do
      specify do
        expect{subject.version}.to_not raise_error
      end
    end

    describe '#description' do
      it 'has a description' do
        expect{subject.class.description}.to_not raise_error
      end
    end

    describe '#human_readable_name' do
      it 'has a human readable name' do
        expect{subject.class.human_readable_name}.to_not raise_error
      end
    end
  end
end