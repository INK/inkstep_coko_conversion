require 'spec_helper'
require 'coko_conversion/ink_step/coko/xsweet_pipeline/docx_extract/docx_to_html_extract_step'
require 'coko_conversion/ink_step/coko/xsweet_pipeline/download_and_execute_xsl_via_saxon_on_docx'

describe InkStep::Coko::XsweetPipeline::DownloadAndExecuteXslViaSaxonOnDocx do

  let(:target_file_name)      { "basic_doc.docx" }
  let(:target_file)           { File.join(Dir.pwd, "spec", "fixtures", "files", "xsweet_pipeline", target_file_name) }
  let(:input_directory)    { File.join(temp_directory, "input_files") }

  before do
    create_directory_if_needed(input_directory)
    FileUtils.cp(target_file, input_directory)
    @subject = InkStep::Coko::XsweetPipeline::DownloadAndExecuteXslViaSaxonOnDocx.new(chain_file_location: temp_directory, position: 1)
  end

  after :each do
    FileUtils.rm_rf(FileUtils.rm_rf(Dir.glob("#{temp_directory}/*")))
  end

  describe '#perform_step' do
    context 'when converting a HTML file' do

      let(:xsl_file)              { File.read('spec/fixtures/files/xsweet_pipeline/docx-html-extract.xsl') }
      let(:remote_uri)            { "https://gitlab.coko.foundation/XSweet/XSweet/raw/ink-api-publish/docx-html-extract.xsl" }

      before do
        stub_request(:get, remote_uri).
            with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>'Ruby'}).
            to_return(:status => 200, :body => xsl_file, :headers => {})
      end

      it 'should return a result for the first test document' do
        @subject.combined_parameters = {remote_xsl_uri: remote_uri}

        @subject.perform_step

        result = File.read(File.join(@subject.send(:working_directory), "basic_doc.html"))
        expect(result).to eq File.read('spec/fixtures/files/xsweet_pipeline/xsweet_1_extract_result.html')
      end
    end
  end

  describe '#version' do
    specify do
      expect{@subject.version}.to_not raise_error
    end
  end

  describe '#description' do
    it 'has a description' do
      expect{@subject.class.description}.to_not raise_error
    end
  end

  describe '#human_readable_name' do
    it 'has a human readable name' do
      expect{@subject.class.human_readable_name}.to_not raise_error
    end
  end
end