require 'spec_helper'
require 'coko_conversion/ink_step/coko/vivliostyle_html_to_pdf_step'

describe InkStep::Coko::VivliostyleHtmlToPdfStep do

  let(:target_file_name)      { "some_text.html" }
  let(:target_file)           { File.join(Dir.pwd, "spec", "fixtures", "files", target_file_name) }

  let!(:input_directory)    { File.join(temp_directory, "input_files") }

  before do
    create_directory_if_needed(input_directory)
    FileUtils.cp(target_file, input_directory)
    @subject = InkStep::Coko::VivliostyleHtmlToPdfStep.new(chain_file_location: temp_directory, position: 1)
  end

  after :each do
    FileUtils.rm_rf(FileUtils.rm_rf(Dir.glob("#{temp_directory}/*")))
  end

  # my tests just hang
  xdescribe '#perform_step' do
    context 'when an input path is specified' do

      context 'and that file exists' do
        it 'finds and converts only that file' do
          expect_successful_conversion(test_subject: @subject)
        end
      end

      context 'and that file does not exist' do
        it 'fails' do
          @subject.combined_parameters = {input_file_path: File.join("html_files", target_file_name)}

          expect{@subject.perform_step}.to raise_error("Cannot find source file html_files/some_text.html")
        end
      end
    end

    context 'when an input path is not specified' do
      before do
        create_directory_if_needed(html_file_directory)
      end

      context 'and there is only one html file' do
        before do
          FileUtils.cp(target_file, working_directory)
        end

        it 'finds and converts that file' do
          expect_successful_conversion(test_subject: @subject)
        end
      end

      context 'and there is only one htm file' do
        before do
          FileUtils.cp(target_file, File.join(working_directory, "some_file.htm"))
        end

        it 'finds and converts that file' do
          expect_successful_conversion(test_subject: @subject)
        end
      end


      context 'and there are >1 html files' do
        before do
          FileUtils.cp(target_file, html_file_directory)
          FileUtils.cp(target_file, working_directory)
        end

        it 'fails' do
          expect{@subject.perform_step}.to raise_error("Multiple files found matching extensions [\"html\", \"htm\"] - please specify which one using options[:input_file_path]")
        end
      end

      context 'and there are no html files' do

        it 'fails' do
          expect{@subject.perform_step}.to raise_error("No HTML files found")
        end

      end
    end

    context 'converting a file successfully using Vivliostyle Electron' do
      before do
        FileUtils.cp(target_file, working_directory)
      end

      specify do
        expect_successful_conversion(test_subject: @subject)
      end
    end
  end

  describe '#version' do
    specify do
      expect{@subject.version}.to_not raise_error
    end
  end

  describe '#description' do
    it 'has a description' do
      expect{@subject.class.description}.to_not raise_error
    end
  end

  describe '#human_readable_name' do
    it 'has a human readable name' do
      expect{@subject.class.human_readable_name}.to_not raise_error
    end
  end
end