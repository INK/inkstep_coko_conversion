# InkStep CokoConversion.
_by the Collaborative Knowledge Foundation_

This is a step gem that is meant to run as a plugin for [the Ink API](https://gitlab.coko.foundation/INK/ink-api). See [the Ink documentation](https://gitlab.coko.foundation/INK/ink-api) for detailed instructions for installing this plugin into an instance of INK.

This gem includes a couple of ways to do conversion, each of which has its own step class.

## Pandoc conversion steps

The steps require Pandoc to be installed on the host machine for them to run properly.

Installation: http://pandoc.org/installing.html
Usage directions and options (lots): http://pandoc.org/getting-started.html

## Calibre conversion steps

Calibre must be installed before these steps will work.

Installation: https://calibre-ebook.com/download
Usage directions and options (lots): https://manual.calibre-ebook.com/generated/en/ebook-convert.html
 
## Mogrify conversion step

On Ubuntu, use `mogrify` instead of `magick`
https://www.imagemagick.org/script/command-line-processing.php

## PDFTK conversion step

PDFTK Server installation:

https://www.pdflabs.com/tools/pdftk-server/

https://packages.ubuntu.com/xenial/pdftk

Man page: https://www.pdflabs.com/docs/pdftk-man-page/

## HTML Tidy conversion step

HTML Tidy installation: http://www.html-tidy.org/

## XSweet XSL pipeline

The XSweet steps all use [Coko's Docx to HTML XSL sheets](https://gitlab.coko.foundation/XSweet/XSweet/tree/ink-api-publish). The step will download and run the latest in the `ink-api-production` branch. The steps are all meant to be run in sequence.

## Vivliostyle and WkHtmlToPdf 

If you want to run the PDF conversion steps, you'll need to get one or both of the following up and running on your INK server:

Charlie Ablett

Collaborative Knowledge Foundation

charlie@coko.foundation